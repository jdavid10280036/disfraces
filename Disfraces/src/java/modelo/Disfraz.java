package modelo;

/**
 *
 * @author Israel
 */
public class Disfraz {
    private int id;
    private String nombre;
    private String descripcion;
    private String talla;
    private double precio;
    private int existencia;
    private int categoria_id;

    public Disfraz(String nombre, String descripcion, String talla, double precio, int existencia, int categoria_id) {
        this.nombre = nombre;
        this.descripcion = descripcion;
        this.talla = talla;
        this.precio = precio;
        this.existencia = existencia;
        this.categoria_id = categoria_id;
    }

    /**
     * @return the id
     */
    public int getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * @return the nombre
     */
    public String getNombre() {
        return nombre;
    }

    /**
     * @param nombre the nombre to set
     */
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    /**
     * @return the talla
     */
    public String getTalla() {
        return talla;
    }

    /**
     * @param talla the talla to set
     */
    public void setTalla(String talla) {
        if(talla.length() <= 2){
            this.talla = talla;
        }
    }

    /**
     * @return the descripcion
     */
    public String getDescripcion() {
        return descripcion;
    }

    /**
     * @param descripcion the descripcion to set
     */
    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    /**
     * @return the precio
     */
    public double getPrecio() {
        return precio;
    }

    /**
     * @param precio the precio to set
     */
    public void setPrecio(double precio) {
        this.precio = precio;
    }

    /**
     * @return the existencia
     */
    public int getExistencia() {
        return existencia;
    }

    /**
     * @param existencia the existencia to set
     */
    public void setExistencia(int existencia) {
        this.existencia = existencia;
    }

    /**
     * @return the categoria_id
     */
    public int getCategoria_id() {
        return categoria_id;
    }

    /**
     * @param categoria_id the categoria_id to set
     */
    public void setCategoria_id(int categoria_id) {
        this.categoria_id = categoria_id;
    }
}

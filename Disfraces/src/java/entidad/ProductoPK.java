/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package entidad;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;

/**
 *
 * @author Israel
 */
@Embeddable
public class ProductoPK implements Serializable {
    @Basic(optional = false)
    @Column(name = "id")
    private int id;
    @Basic(optional = false)
    @NotNull
    @Column(name = "categoria_id")
    private short categoriaId;

    public ProductoPK() {
    }

    public ProductoPK(int id, short categoriaId) {
        this.id = id;
        this.categoriaId = categoriaId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public short getCategoriaId() {
        return categoriaId;
    }

    public void setCategoriaId(short categoriaId) {
        this.categoriaId = categoriaId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) id;
        hash += (int) categoriaId;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ProductoPK)) {
            return false;
        }
        ProductoPK other = (ProductoPK) object;
        if (this.id != other.id) {
            return false;
        }
        if (this.categoriaId != other.categoriaId) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entidad.ProductoPK[ id=" + id + ", categoriaId=" + categoriaId + " ]";
    }
    
}

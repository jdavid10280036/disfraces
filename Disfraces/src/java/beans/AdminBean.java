/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package beans;

import entidad.Admin;
import java.util.Collection;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import jsf.*;

/**
 *
 * @author Israel
 */
@ManagedBean
@SessionScoped
public class AdminBean {
    private String user;
    private String password;
    private String nombre;
    private String apellidos;
    private int tipo;
    
    public String login(){
        //Buscar(user, password);
        
//        AdminBean e = new AdminBean(); // = Busca(user, password)
//       
        AdminController ac = new AdminController();
        Admin a = ac.buscar(user, password);
        
        
//        if(a != null){
//            if(a.getTipo() == 1){
//                return "admin";
//            }else{
//                return "empleado";
//            }
//        }else{
//            return "intruso";
//        }
        return "intruso";
    }

    public String getUser() {
        return user;
    }
    public void setUser(String user) {
        this.user = user;
    }

    public String getPassword() {
        return password;
    }
    public void setPassword(String password) {
        this.password = password;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellidos() {
        return apellidos;
    }

    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    public int getTipo() {
        return tipo;
    }

    public void setTipo(int tipo) {
        this.tipo = tipo;
    }
}
